interface Triangle {
    width: number;
    length: number;
    area?: number;
}

function calculateTriangleArea(triangle: Triangle) {
    triangle.area = (triangle.width * triangle.length) / 2;
}

const firstTriangle: Triangle = { width: 7.0, length: 3.5 };
const secondTriangle: Triangle = { width: 4.3, length: 6.4 };
const thirdTriangle: Triangle = { width: 5.5, length: 5.0 };

calculateTriangleArea(firstTriangle);
calculateTriangleArea(secondTriangle);
calculateTriangleArea(thirdTriangle);

console.log("Area of the first triangle:", firstTriangle.area);
console.log("Area of the second triangle:", secondTriangle.area);
console.log("Area of the third triangle:", thirdTriangle.area);

function findLargestAreaTriangle(triangles: Triangle[]) {
    let largestArea = 0;
    let largestTriangleIndex = -1;
    for (let i = 0; i < triangles.length; i++) {
        if ((triangles[i].area ?? 0 )> largestArea) {
            largestArea = triangles[i].area ?? 0;
            largestTriangleIndex = i;
        }
    }
    if (largestTriangleIndex !== -1) {
        console.log(`Triangle ${largestTriangleIndex + 1} has the biggest area!`);
    }
}

findLargestAreaTriangle([firstTriangle, secondTriangle, thirdTriangle]);