/*interface Boat {
    hullBreached: boolean
    fillLevel: number
    isItSinking: boolean
    sunk: boolean
}*/

class Boat {
    hullBreached: boolean;
    fillLevel: number;
    sunk: boolean;

    constructor(hullBreached: boolean, fillLevel: number) {
        this.hullBreached = hullBreached;
        this.fillLevel = fillLevel;
        this.sunk = false;
    }

    isItSinking(): void {
        if (this.hullBreached) {
            console.log("The hull of the boat has been breached.");
            while (this.fillLevel < 100) {
                this.fillLevel += 20;
                console.log(`The boat is filling up: ${this.fillLevel}%`);
                if (this.fillLevel >= 100) {
                    this.sunk = true;
                    console.log("The boat has sunk.");
                }
            }
        } else {
            console.log("The boat is not sinking. Hull integrity is maintained.");
        }
    }
}

const myBoat = new Boat(true, 0);

myBoat.isItSinking();

console.log(myBoat);
