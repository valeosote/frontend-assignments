const readable = "asdf qwerty";
const asASCII: Array<number> = readable.split("").map(c => c.charCodeAt(0));
console.log(asASCII);

const fixthis: Array<string> = [
    "The quick brown fox jumps over the lazy dog.",
    "Pack my box with five dozen liquor jugs.",
    "Mr Jock, TV quiz PhD, bags few lynx.",
    "cwm fjord bank glyphs vext quiz.",
    "Jackdaws love my big sphinx of quartz.",
    "How razorback-jumping frogs can level six piqued gymnasts!",
    "sphinx of black quartz, judge my vow.",
    "The five boxing wizards jump quickly.",
    "quick fox jumps nightly above wizard.",
    "Six big devils from Japan quickly forgot how to waltz."
];

const fixedIt = fixthis.sort((a, b) => a.localeCompare(b));
console.log(fixedIt);
