function a006130(n: number): number {
    if (n === 0 || n === 1){
        return n;
    } else {
        return (a006130(n - 2) * 3) + a006130(n - 1);
    }
}

console.log(a006130(17));
