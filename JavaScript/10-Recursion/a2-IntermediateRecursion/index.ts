function sentencify(given: Array<string>, starti: number): string {
    if (starti+1 >= given.length){
        return given[starti] + "!";
    } else {
        return given[starti] + " " + sentencify(given, starti+1);
    }
}

const wordArray = [ "The", "quick", "silver", "wolf" ];

console.log(sentencify(wordArray, 0)); // prints "The quick silver wolf!"
console.log(sentencify(wordArray, 1)); // prints "quick silver wolf!"
