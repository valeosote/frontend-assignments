interface Thingthing {
    name: string;
    width: number;
    height: number;
    children: Thingthing[];
}

function buildUserInterface() {
    const mainWindow: Thingthing = { name: "MainWindow", width: 600, height: 400, children: [ ] };
    const buttonExit: Thingthing = { name: "ButtonExit", width: 100, height: 30, children: [ ] };
    mainWindow.children.push(buttonExit);

    const settingsWindow: Thingthing = { name: "SettingsWindow", width: 400, height: 300, children: [ ] };
    const buttonReturnToMenu: Thingthing = { name: "ButtonReturnToMenu", width: 100, height: 30, children: [ ] };
    settingsWindow.children.push(buttonReturnToMenu);
    mainWindow.children.push(settingsWindow);

    const profileWindow: Thingthing = { name: "ProfileWindow", width: 500, height: 400, children: [ ] };
    const profileInfoPanel: Thingthing = { name: "ProfileInfoPanel", width: 200, height: 200, children: [ ] };
    profileWindow.children.push(profileInfoPanel);
    mainWindow.children.push(profileWindow);

    return mainWindow;
}

const userInterfaceTree = buildUserInterface();

console.log(userInterfaceTree);
console.log(userInterfaceTree.children[1].children);
console.log(userInterfaceTree.children[2].children);


// tätä osaa katsoin mallivastauksista, kun ei ihan auennut mitä tässä ajettiin takaa
function findControl(control:Thingthing, nameToFind: string): Thingthing | null {
    if (control.name === nameToFind) {
        return control;
    }
    for (const child of control.children) {
        const result = findControl(child, nameToFind);

        if (result !== null) {
            return result;
        }
    }
    return null;
}


const profileInfoPanel = findControl(userInterfaceTree, "ProfileInfoPanel");
profileInfoPanel!.width += 100;
console.log(profileInfoPanel); // prints { name: 'ProfileInfoPanel', width: 300, height: 200, children: [] }
