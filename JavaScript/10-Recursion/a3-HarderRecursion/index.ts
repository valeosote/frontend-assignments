function mergeSort(given: Array<any>): Array<any> {
    if (given.length <= 1){
        return given;
    } else {
        const puolikas = Math.floor(given.length / 2);
        return mergeSublists(mergeSort(given.slice(0,puolikas)),mergeSort(given.slice(puolikas)));
    }
}

function mergeSublists(leftlist: Array<any>, rightlist: Array<any>): Array<any> {
    const palautettava: Array<any> = [];
    let osoitinL = 0;
    for (const osoitinR in rightlist){
        while (osoitinL < leftlist.length && leftlist[osoitinL] < rightlist[osoitinR]){
            palautettava.push(leftlist[osoitinL++]);
            //console.log(osoitinL, osoitinR, leftlist, rightlist, palautettava);
        }
        palautettava.push(rightlist[osoitinR]);
    }
    while (osoitinL < leftlist.length){
        palautettava.push(leftlist[osoitinL++]);
    }
    return palautettava;
}

const array = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
const sorted = mergeSort(array);
console.log(sorted); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]

// EXTRA-extra
const arrayX = [ 4, 19, 7, 1, 9, 22, 6, 13, -4 ];
console.log(mergeSort(arrayX)); // prints [ -4, 1, 4, 6, 7, 9, 13, 19, 22 ]