interface CommandHandler {
    (): void
}
class Robot {
    x: number
    y: number
    private commandHandlers: { [key: string]: CommandHandler }

    constructor() {
        this.x = 0;
        this.y = 0;
        this.commandHandlers = {
            N: () => this.y++,
            E: () => this.x++,
            S: () => this.y--,
            W: () => this.x--,
            C: () => {}
        }
    }
    handleCommandList(commands: string): void {
        for (const c of commands.split("")) {
            if (c === "B") {
                break;
            }
            if (Object.keys(this.commandHandlers).includes(c)){
                this.commandHandlers[c]();
            }
        }
    }
    toString(): string {
        return `{ x: ${this.x}, y: ${this.y} }`;
    }
}

const robot1 = new Robot();

const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEEXSWWNNNEEEBENNNEEE";

robot1.handleCommandList(commandList);

console.log(robot1.toString());
