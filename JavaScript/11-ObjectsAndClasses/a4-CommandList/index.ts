interface Robot {
    x: number
    y: number
}

/*interface CommandHandler {
    /[key: string]: (...args: [Robot]) => void;
}*/

/*type CommandHandler = (botti: Robot) => void;*/

interface CommandHandler {
    (instance: Robot): void
}

const robot1: Robot = { x: 0, y: 0 };

const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

const commandHandlers: { [key: string]: CommandHandler } = {
    N: (botti) => botti.y++,
    E: (botti) => botti.x++,
    S: (botti) => botti.y--,
    W: (botti) => botti.x--,
    C: (botti) => {}
}

function processCommands(commmands: string, botti: Robot): void {
    for (const c of commandList.split("")) {
        if (c === "B") {
            break;
        }
        commandHandlers[c](botti);
    }
}

processCommands(commandList, robot1);
console.log(robot1);






/* some notes on an earlier thing misplaced into this file I guess
//const games: Array<{[key: string]: string | number | boolean}> = [
    
const games = [
    { id: 1586948654, date: "2022-10-27", score: 145, won: false },
    { id: 2356325431, date: "2022-10-30", score: 95, won: false },
    { id: 2968411644, date: "2022-10-31", score: 180, won: true },
    { id: 1131684981, date: "2022-11-01", score: 210, won: true },
    { id: 1958468135, date: "2022-11-01", score: 111, won: true },
    { id: 2221358512, date: "2022-11-02", score: 197, won: false },
    { id: 1847684969, date: "2022-11-03", score: 203, won: true }
];

console.log(games.filter(g => g.won).map(g => g.score).reduce((a,c,i) => a-(a-c)/(i+1)));

console.log(games.filter(g => !g.won).map(g => g.score).reduce((a,c,i) => (a*i+c)/(i+1)));

games.filter(g => g.won).map(g => g.score).reduce((a,c,i) => a-(a-c)/(i+1))

// scores.reduce((a,c,i) => a-(a-c)/(i+1));

games.filter(g => g.won).reduce((a,c,i) => a-(a-c.score)/(i+1), 0)

// games.filter(g => g.won).reduce((a,c,i) => a-(a-c.score)/(i+1), c.score)*/