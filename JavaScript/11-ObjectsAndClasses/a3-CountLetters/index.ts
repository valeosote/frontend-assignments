function getCountOfLetters(here: string): {[key: string]: number} {
    const pala: {[key: string]: number} = {};
    for (let i = 0; i<here.length; i++){
        if (here.charAt(i).trim() === "") {
            continue;
        }
        if (here.charAt(i) in pala){
            pala[here.toLowerCase().charAt(i)]++;
        } else {
            pala[here.toLowerCase().charAt(i)] = 1;
        }
    }
    return pala;
}

const randomSentence = "The cat in the hat sat on the mat";

//const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

console.log(getCountOfLetters(randomSentence));