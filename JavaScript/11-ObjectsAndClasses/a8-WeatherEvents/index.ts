class WeatherEvent {
    timestamp: Date;

    constructor(timestamp: Date) {
        this.timestamp = timestamp;
    }

    getInformation(): string {
        return "";
    }

    print(): void {
        console.log(`${this.timestamp.toISOString()} ${this.getInformation()}`);
    }
}

class TemperatureChangeEvent extends WeatherEvent {
    temperature: number;

    constructor(timestamp: Date, temperature: number) {
        super(timestamp);
        this.temperature = temperature;
    }

    getInformation(): string {
        return `temperature: ${this.temperature} °C`;
    }
}

class HumidityChangeEvent extends WeatherEvent {
    humidity: number;

    constructor(timestamp: Date, humidity: number) {
        super(timestamp);
        this.humidity = humidity;
    }

    getInformation(): string {
        return `humidity: ${this.humidity} %`;
    }
}

class WindStrengthChangeEvent extends WeatherEvent {
    windStrength: number;

    constructor(timestamp: Date, windStrength: number) {
        super(timestamp);
        this.windStrength = windStrength;
    }

    getInformation(): string {
        return `wind strength: ${this.windStrength} m/s`;
    }
}

const weatherEvents: WeatherEvent[] = [];

weatherEvents.push(new TemperatureChangeEvent(new Date("2022-11-29 03:00"), -6.4));
weatherEvents.push(new HumidityChangeEvent(new Date("2022-11-29 04:00"), 95));
weatherEvents.push(new WindStrengthChangeEvent(new Date("2022-11-30 13:00"), 2.2));

weatherEvents.forEach(weatherEvent => weatherEvent.print());
// Should print:
// 2022-11-29 03:00 temperature: -6.4°C
// 2022-11-29 04:00 humidity: 95%
// 2022-11-30 13:00 wind strength: 2.2 m/s
