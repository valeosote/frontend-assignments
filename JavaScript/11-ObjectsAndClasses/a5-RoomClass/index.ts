class Room {
    width: number;
    height: number;
    furniture: Array<string>;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
        this.furniture = [];
    }

    getArea() {
        return this.width * this.height;
    }

    addFurniture(huonekalu: string){
        this.furniture.push(huonekalu);
    }
}

const roomA = new Room(4,5);

console.log(roomA);

const roomB = new Room(4.5, 6.0);

console.log(roomB);
console.log(roomB.getArea());

const roomC = new Room(4.5, 6.0);
roomC.addFurniture("sofa");
roomC.addFurniture("bed");
roomC.addFurniture("chair");
console.log(roomC); // prints Room { width: 4.5, height: 6, furniture: [ 'sofa', 'bed', 'chair' ] }