const sanasto:  { [key: string]: string } = {
    hello: "hei",
    world: "maailma",
    bit: "bitti",
    byte: "tavu",
    integer: "kokonaisluku",
    boolean: "totuusarvo",
    tring: "merkkijono",
    network: "verkko"
}

function printTranslatableWords() {
    console.log(Object.keys(sanasto));
}

printTranslatableWords();

function translate(sana: string): string {
    return sanasto[sana] || `No translation exists for the word "${sana}" given as the argument.`;
}

console.log(translate("world"));
console.log(translate("wind"));