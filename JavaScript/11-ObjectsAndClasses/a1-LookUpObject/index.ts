function calculateTotalScore(sis: string): number {
    const luo: { [key: string]: number } = {
        S: 8,
        A: 6,
        B: 4,
        C: 3,
        D: 2,
        F: 0
    }
    return sis.split("").reduce((a,c) => a + luo[c], 0)
}

const totalScore = calculateTotalScore("DFCBDABSB");
console.log(totalScore); // prints 33

function calculateAverageScore(sis: string): number {
    return calculateTotalScore(sis)/sis.length;
}

const averageScore = calculateAverageScore("DFCBDABSB");
console.log(averageScore); // prints 3.6666666666666665

const somegrades = [ "AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC" ];

const somescores = somegrades.map(calculateAverageScore);

console.log(somescores);