function incrementAll(luvut: number[]): number[] {
    return luvut.map(n => n+1);
}

const numbers = [ 4, 7, 1, 8, 5 ];
const newNumbers = incrementAll(numbers);
console.log(newNumbers); // prints [ 5, 8, 2, 9, 6 ]

function decrementAll(luvut: number[]): number[] {
    return luvut.map(n => n-1);
}

const newerNumbers = decrementAll(numbers);
console.log(newerNumbers); // prints [ 3, 6, 0, 7, 4 ]