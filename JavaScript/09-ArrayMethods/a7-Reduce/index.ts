// https://coursework.vschool.io/array-reduce-exercises/

// 1
function total(luvut: number[]): number {
    return luvut.reduce((a,c) => a+c)
}

console.log(total([1,2,3])); // 6

// 2
function stringConcat(luvut: number[]): string {
    return luvut.reduce((a,c) => a+c,"")
}

console.log(stringConcat([1,2,3])); // "123"

// 3
interface Voter {
    name: string
    age: number
    voted: boolean
}

const voters: Voter[] = [
    {name:'Bob' , age: 30, voted: true},
    {name:'Jake' , age: 32, voted: true},
    {name:'Kate' , age: 25, voted: false},
    {name:'Sam' , age: 20, voted: false},
    {name:'Phil' , age: 21, voted: true},
    {name:'Ed' , age:55, voted:true},
    {name:'Tami' , age: 54, voted:true},
    {name: 'Mary', age: 31, voted: false},
    {name: 'Becky', age: 43, voted: false},
    {name: 'Joey', age: 41, voted: true},
    {name: 'Jeff', age: 30, voted: true},
    {name: 'Zack', age: 19, voted: false}
];

function totalVotes(peep: Voter[]): number {
    return peep.reduce((a,c) => a+(+c.voted),0)
}

console.log(totalVotes(voters)); // 7

// 4
interface Purchase {
    title: string
    price: number
}

const wishlist = [
    { title: "Tesla Model S", price: 90000 },
    { title: "4 carat diamond ring", price: 45000 },
    { title: "Fancy hacky Sack", price: 5 },
    { title: "Gold fidgit spinner", price: 2000 },
    { title: "A second Tesla Model S", price: 90000 }
];

function shoppingSpree(ostokset: Purchase[]): number {
    return ostokset.reduce((a,ostos) => a+ostos.price,0)
}

console.log(shoppingSpree(wishlist)); // 227005

// 5
function flatten(listat: any[][]): any[] {
    return listat.reduce((a,c) => a.concat(c), []);
}

const arrays: any[][] = [
    ["1", "2", "3"],
    [true],
    [4, 5, 6]
];

console.log(flatten(arrays)); // ["1", "2", "3", true, 4, 5, 6];

// 6
interface Votes {
    numYoungVotes: number
    numYoungPeople: number
    numMidVotes: number
    numMidPeople: number
    numOldVotes: number
    numOldPeople: number     
}

function voterResults(casters: Voter[]): Votes {
    return casters.reduce( (a,c) => {
        if (c.age < 26){
            a.numYoungPeople++;
            c.voted ? a.numYoungVotes++ : undefined;
        } else if (c.age < 36){
            a.numMidPeople++;
            c.voted ? a.numMidVotes++ : undefined;
        } else {
            a.numOldPeople++;
            c.voted ? a.numOldVotes++ : undefined;
        }
        return a;
    },{ numYoungVotes: 0,
        numYoungPeople: 0,
        numMidVotes: 0,
        numMidPeople: 0,
        numOldVotes: 0,
        numOldPeople: 0
    })
}

console.log(voterResults(voters)); // Returned value shown below:
/*
{ numYoungVotes: 1,
  numYoungPeople: 4,
  numMidVotesPeople: 3,
  numMidsPeople: 4,
  numOldVotesPeople: 3,
  numOldsPeople: 4 
}
*/

// 7
import axios from 'axios';

axios.get(`https://api.github.com/users/M11k4/repos`)
    .then(response => {
        const repositories: { watchers_count: string }[] = response.data;
        const totalWatchers = repositories.reduce((a, c) => a + parseInt(c.watchers_count), 0);
        console.log('Watchers across my repositories on Github:', totalWatchers);
    })
    .catch(error => {
        console.error('Error:', error);
});
