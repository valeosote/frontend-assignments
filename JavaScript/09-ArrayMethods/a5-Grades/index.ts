interface Student {
    name: string
    score?: number
    grade?: number
}

const students = [ { name: "Sami", score: 24.75 },
                   { name: "Heidi", score: 20.25 },
                   { name: "Jyrki", score: 27.5 },
                   { name: "Helinä", score: 26.0 },
                   { name: "Maria", score: 17.0 },
                   { name: "Yrjö", score: 14.5  } ];

function grader(arvo: number): number {
    if(arvo > 26){
        return 5;
    } else if (arvo > 23){
        return 4;
    } else if (arvo > 20){
        return 3;
    } else if (arvo > 17){
        return 2;
    } else if (arvo >= 14){
        return 1;
    } else
        return 0;
}

function getGrades(oppilaat: Student[]): Student[] {
    return oppilaat.map(student => ({ name: student.name, grade: grader(student.score!)}))
}

console.log(getGrades(students));