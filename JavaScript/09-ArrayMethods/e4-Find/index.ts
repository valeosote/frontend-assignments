// Create a program that finds us the first animal that ends in letter ‘t’.

const animals: string[] = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

console.log(animals.find(animal => animal.endsWith("t")));

// EXTRA: Find the first animal that ends in letter ‘y’ and starts with letter ‘d’.

console.log(animals.find(animal => animal.startsWith("d") && animal.endsWith("y")));