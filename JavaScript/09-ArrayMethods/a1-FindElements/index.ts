const numerot = [ 8, 12, 17, 9, 16, 24, 16, 25, 35, 27, 38, 50 ];

// Using a loop, find the first number that is above 20 and print it.

let i = 0;
while (numerot[i]<=20){
    i++;
}
console.log(numerot[i])

// Do the same as in a), but use Array.find instead.

console.log(numerot.find(n => n>20));

// Using Array.findIndex, find the index of the first number that is above 20 and print the index.

const poikki = numerot.findIndex(n => n>20);
console.log(poikki);

// Using Array.splice, remove all elements that come after the index that you found in c). Afterwards, print the whole array.

numerot.splice(poikki+1);
console.log(numerot);