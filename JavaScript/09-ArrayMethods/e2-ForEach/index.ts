const numbers: number[] = [5, 13, 2, 10, 8];

console.log(numbers);

let product: number = 1;
numbers.forEach(num =>product *= num);
console.log("Product of the numbers:", product);

let sum: number = 0;
numbers.forEach(num => sum += num);
const average: number = sum / numbers.length;
console.log("Average of the numbers:", average);
