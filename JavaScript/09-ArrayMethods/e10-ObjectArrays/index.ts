//Use the array methods to create new arrays.
//List the last names of all employees
//List all employee objects that are Full Stack Residents
//List all Instructors’ full names (e.g., “Wes Reid”)

interface User {
    firstName: string
    lastName: string
    role: string
}

const users = [{ firstName: 'Bradley', lastName: 'Bouley', role: 'Full Stack Resident' },
               { firstName: 'Chloe', lastName: 'Alnaji', role: 'Full Stack Resident' },
               { firstName: 'Jonathan', lastName: 'Baughn', role: 'Enterprise Instructor' },
               { firstName: 'Michael', lastName: 'Herman', role: 'Lead Instructor' },
               { firstName: 'Robert', lastName: 'Hajek', role: 'Full Stack Resident' },
               { firstName: 'Wes', lastName: 'Reid', role: 'Instructor'},
               { firstName: 'Zach', lastName: 'Klabunde', role: 'Instructor'}];

const employees = users.map(user => user.lastName);
console.log(employees);

const fullStackResidents = users.filter(user => user.role === "Full Stack Resident");
console.log(fullStackResidents);

const fullNames = users.map(user => `${user.firstName} ${user.lastName}`);
console.log(fullNames);