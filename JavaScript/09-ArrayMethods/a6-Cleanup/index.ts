 interface Olio {
    x: number
    y: number
    type: string
    toDelete: boolean
}

const objectArray: Array<Olio | null> =
                    [ { x: 14, y: 21, type: "tree", toDelete: false },
                      { x: 1, y: 30, type: "house", toDelete: false },
                      { x: 22, y: 10, type: "tree", toDelete: true },
                      { x: 5, y: 34, type: "rock", toDelete: true },
                      null,
                      { x: 19, y: 40, type: "tree", toDelete: false },
                      { x: 35, y: 35, type: "house", toDelete: false },
                      { x: 19, y: 40, type: "tree", toDelete: true },
                      { x: 24, y: 31, type: "rock", toDelete: false } ];

const objectArray2 = objectArray.map(olio => (olio && !olio.toDelete) ? olio : null);

for (const i in objectArray){
    if ( objectArray[i] && objectArray[i]!.toDelete){
        objectArray[i] = null;
    }
}

console.log(objectArray);
console.log(objectArray2);

// Imagine that instead of 9 entries, the above array would have 100,000 entries.
// What would be the implications for performance and memory use between the
// two methods used?

// Paikallaan tehtävät muutokset eivät vie niin paljon muistiresursseja.
// Luultavasti nämä erot eivät merkitsisi vielä hirveästi edes 100,000
// alkiolla, jos tehtävää ei tarvitse suorittaa kuin muutaman kerran.
// Toki tämä ero on hyvä tiedostaa sitten kun tulee rajat vastaan.
