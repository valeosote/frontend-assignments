// Create a program that prints the names of all animals that have the letter ‘e’ in their name.

const animals: string[] = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

// implementation using a for...of loop
for (const animal of animals) {
    if (animal.includes("e")) {
        console.log(animal);
    }
}

// implementation using a ForEach loop
animals.forEach(animal => animal.includes("e") ? console.log(animal) : undefined);
