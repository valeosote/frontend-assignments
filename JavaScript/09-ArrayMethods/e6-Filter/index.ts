// Create a program that finds us all animals that
// a) have the letter ‘o’ in their name.
// b) don’t have a ‘h’ or ‘o’ in their name!

const animals: string[] = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

const animalsO = animals.filter(animal => animal.includes("o"));
const animalsH = animals.filter(animal => !animal.includes("h") && !animal.includes("o") );

console.log(animalsO);
console.log(animalsH);