// finds us the index of the first animal that has 6 or more letters in its name and prints the index

const animals: string[] = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

const kivaIndeksi: number | undefined = animals.findIndex(animal => animal.length >= 6);

console.log(`The animal at index ${kivaIndeksi} is ${animals[kivaIndeksi]}. It is the first animal with a length greater than five.`);

/*
const lause = "Can you please answer this question with exactly fourteen words and seventy five letters";
const sanat = lause.split(" ");
console.log(lause)
console.log(`There are ${sanat.length} words in the sentence.`)
console.log(`It has ${lause.replace(/\s/g, "").length} letters.`)*/
