// Using map,
//create a new array that contains the lengths of the animals’ names
//create a new array that contains booleans that tell whether the specific animals have the letter ‘o’ as their second letter

const animals: string[] = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];
console.log(animals);

const pituudet: number[] = animals.map(animal => animal.length);
console.log(pituudet);

const ollakko: boolean[] = animals.map(animal => animal[1] === "o");
console.log(ollakko);