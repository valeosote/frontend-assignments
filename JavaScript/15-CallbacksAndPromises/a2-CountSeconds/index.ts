function delay(milliseconds: number): Promise<void> {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

async function waitFor(milliseconds: number): Promise<void> {
    await delay(milliseconds);
}

/* async function example() {
    console.log("Waiting for 2 seconds...");
    await waitFor(2000);
    console.log("2 seconds have passed.");
}

example(); */

async function countSeconds(): Promise<void> {
    for (let i = 0; i <= 10; i++) {
        console.log(i);
        await waitFor(1000);
    }
}

countSeconds();
