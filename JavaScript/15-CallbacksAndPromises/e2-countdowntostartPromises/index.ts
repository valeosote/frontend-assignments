function countdown(seconds: number): Promise<void> {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return new Promise<void>((resolve, _reject) => {
        let count = seconds;

        function tick() {
            if (count > 0) {
                setTimeout(() => {
                    console.log(`${".".repeat(seconds - count)}${count}`);
                    count--;
                    tick();
                }, 1000);
            } else {
                setTimeout(() => {
                    resolve();
                }, 1000);
            }
        }

        tick();
    });
}

countdown(3)
    .then(() => {
        console.log("GO!");
    });
