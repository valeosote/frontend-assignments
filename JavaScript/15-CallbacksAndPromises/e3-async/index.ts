const getValue = (): Promise<{ value: number }> => {
    return new Promise((res) => {
        setTimeout(() => {
            res({ value: Math.random() });
        }, Math.random() * 1500);
    });
};

function slowdownabit() {
    getValue().then(result1 => {
        getValue().then(result2 => {
            const value1 = result1.value;
            const value2 = result2.value;

            console.log(`The promised value 1 is ${value1} and value 2 is ${value2}.`);
        });
    });
}

slowdownabit();

async function slowdownagain() {
    const result1 = await getValue();
    const result2 = await getValue();

    const value1 = result1.value;
    const value2 = result2.value;

    console.log(`The awaited value 1 is ${value1} and value 2 is ${value2}.`);
}

slowdownagain();