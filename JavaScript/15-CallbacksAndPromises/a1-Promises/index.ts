function sum(limit: number): number{
    if (limit === 0){
        return limit;
    } else {
        return limit + sum(limit-1);
    }
}

console.log(sum(10)); // prints 55 (= 0 + 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10)

function summ(limit: number): number{
    return (limit**2+limit)/2;
}

console.log(summ(10)); // prints 55 (= 0 + 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10)

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const lupaus: Promise<void> = new Promise((resolve, _reject) => {
    resolve(summ(50000));
}).then((result) => {
    console.log(result);
});

new Promise<number>((resolve) => {
    setTimeout(() => {
        resolve(summ(50000));
    }, 2000);
}).then((result) => {
    console.log(result);
});

const createDelayedCalculation = (raja: number, aika: number): Promise<number> => {
    const lupa: Promise<number> = new Promise((resolve) => {
        setTimeout(() => {
            resolve(summ(raja));
        }, aika);
    });
    return lupa;
};
  
createDelayedCalculation(20000000, 2000).then((result) => console.log(result));
createDelayedCalculation(500000, 500).then((result) => console.log(result));

// e
// Why, when running the above code, is the result of sum(50000) printed before sum(20000000),
// despite that the promise for the latter is created above the former?
//
// Both are processed at the same time, but the delay on the former is larger, so the latter
// resolves first. We could also chain them together in such a way that the second call
// is only created after the first one finishes, if this was desired. -Miika