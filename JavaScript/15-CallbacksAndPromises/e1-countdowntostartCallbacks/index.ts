function countdawn(seconds: number): void {
    function startCountdown(i: number): void {
        setTimeout(() => {
            if (i > 0) {
                console.log(`${i}`);
                startCountdown(i - 1);
            } else {
                console.log("GO!");
            }
        }, 1000);
    }
    startCountdown(seconds);
}

countdawn(3);
