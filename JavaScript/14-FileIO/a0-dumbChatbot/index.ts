import readline from "readline-sync";

console.log(`Hi! I am a dumb chat bot You can check all the things I can do by typing 'help'.`);

function helpotus() {
    console.log(`   -----------------------------
    Here´s a list of commands that I can execute! 

    help: Opens this dialog.
    hello: I will say hello to you
    botInfo: I will introduce myself
    botName: I will tell my name
    botRename: You can rename me
    forecast: I will forecast tomorrows weather 100% accurately
    quit: Quits the program.
    -----------------------------`);
}
function helloutus() {
    if (callsign === "") {
        console.log("Hello!");
        console.log("What is your name?");
        callsign = readline.question(`:: `);
    }
    console.log(`Hello there, ${callsign}!`);
}
function botinttaus() {
    console.log(`I am a dumb bot. You can ask me almost anything :). You have already asked me ${crun} questions.`);
    //en erotellut eri komentoja ja niiden laskentaa
}
function bottaus() {
    console.log("Wonderful, you can give me a new name!");
    const nameoption = readline.question(`:: `);
    console.log(`My name used to be ${botname} but now it is ${nameoption}. Are you happy with this choice?`);
    // eslint-disable-next-line no-constant-condition
    renamer: while(true){
        const yesnoans = readline.question(`::`);
        switch (yesnoans){
        case "yes":
            botname = nameoption;
            break renamer;
        case "no":
            console.log("Well, then let's revert to my old name.");
            break renamer;
        case "quit":
            break renamer;
        default:
            console.log("I'm sorry, I was expecting a yes or no answer.");
        }
    }
}
function forecast100() {
    //arpoo aina uuden ennustuksen, kun se oli kivempaa kuin vanhan muistaminen uudelleen kutsuttaessa
    const weatherOptions = [
        "Tomorrow will be sunny with a chance of rain.",
        "Tomorrow will be sunny with clear skies.",
        "Tomorrow will be partly cloudy with a chance of showers.",
        "Tomorrow will be overcast with occasional rain.",
        "Tomorrow will be windy with scattered thunderstorms.",
        "Tomorrow will be foggy conditions with low visibility.",
        "Tomorrow we will get heavy snowfall with blizzard conditions."
    ];
    const randomIndex = Math.floor(Math.random() * weatherOptions.length);
    console.log(weatherOptions[randomIndex]);
    const rand1 = Math.floor(Math.random() ** 2 * 20 * (Math.random() < 0.4 ? 1 : -2)+10);
    const rand2 = Math.floor(Math.random() ** 3 * 25 + 5);
    console.log(`The temperature will range between ${rand1} and ${rand1+rand2} degrees Celsius.`);
}

let callsign = "";
let crun = 0;
let botname = "DefaultBot";

// eslint-disable-next-line no-constant-condition
main: while (true){
    const answer = readline.question(`::: `).trim().toLowerCase();
    crun++;
    switch (answer) {
    case "quit":
        break main;
    case "help":
        helpotus();
        break;
    case "hello":
        helloutus();
        break;
    case "botinfo":
        botinttaus();
        break;
    case "botname":
        console.log(`My name is ${botname}. We can agree to change it though, with the command botRename.`);
        break;
    case "botrename":
        bottaus();
        break;
    case "forecast":
        forecast100();
        break;
    default:
        console.log(`Invalid command. Please type "help" for a list of commands.`);
        break;
    }
}