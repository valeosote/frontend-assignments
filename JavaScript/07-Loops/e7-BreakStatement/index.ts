let cherry = 1; //a.k.a. currentProduct
let multiplicand = 1;
while (true) {
    cherry *= multiplicand;
    if (cherry % 600 === 0) {
        console.log("The smallest number with a factorial divisible by 600 is:", multiplicand);
        break;
    }
    multiplicand++;
}