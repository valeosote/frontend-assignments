// onko nämä nyt parhaat globaalina muuttujina?
let x = 0;
let y = 0;

const commandList: string = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

bigGuy: for (const c of commandList.split("")){
    switch (c) {
        case 'B':
            break bigGuy;
        case 'N':
            y++;
            break;
        case 'E':
            x++;
            break;
        case 'S':
            y--;
            break;
        case 'W':
            x--;
            break;
        case 'C':
            break;
        default:
            console.log("Ehkä olisit voinut antaa paremman komentosarjan.")
    }
}

console.log(`Final position x: ${x} and y: ${y}.`)