let factorial = 1;
const whichNumbersWereUsed: Array<number> = [];
for (let number = 1; number <= 10; number++) {
    if (number % 3 === 0) {
        continue;
    }
    factorial *= number;
    whichNumbersWereUsed.push(number);
}
console.log("Factorial of 10 excluding numbers divisible by 3:", factorial);
console.log(whichNumbersWereUsed);
