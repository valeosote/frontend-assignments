// 0 100 200 300 400 500 600 700 800 900 1000
let sequence1 = ""
for (let i = 0; i <= 1000; i += 100) {
    sequence1 += i + " ";
}
console.log(sequence1);

// 1 2 4 8 16 32 64 128
let sequence2 = "";
for (let i = 1; i <= 128; i *= 2) {
    sequence2 += i + " ";
}
console.log(sequence2);

// 3 6 9 12 15
let sequence3 = "";
for (let i = 3; i <= 15; i += 3) {
    sequence3 += i + " ";
}
console.log(sequence3);

// 9 8 7 6 5 4 3 2 1 0
let sequence4 = "";
for (let i = 9; i >= 0; i--) {
    sequence4 += i + " ";
}
console.log(sequence4);

// 1 1 1 2 2 2 3 3 3 4 4 4
let sequence5 = "";
for (let i = 1; i <= 4; i++) {
    for (let j = 0; j < 3; j++) {
        sequence5 += i + " ";
    }
}
console.log(sequence5);

// 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4
let sequence6 = "";
for (let i = 0; i < 3; i++) {
    for (let j = 0; j <= 4; j++) {
        sequence6 += j + " ";
    }
}
console.log(sequence6);