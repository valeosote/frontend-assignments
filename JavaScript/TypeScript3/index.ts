// Exercise 6: Objects and JSON

import * as fs from "fs";
//import readlineSync from "readline-sync";

interface Book {
    author: string;
    title: string;
    readingStatus: boolean;
    id: number;
}

const library: Book[] = [
    {
        author: "David Wallace",
        title: "Infinite Jest",
        readingStatus: false,
        id: 1,
    },
    {
        author: "Douglas Hofstadter",
        title: "Gödel, Escher, Bach",
        readingStatus: true,
        id: 2,
    },
    {
        author: "Harper Lee",
        title: "To Kill A Mockingbird",
        readingStatus: false,
        id: 3,
    },
];

function getBook(id: number): Book | undefined {
    return library.find(book => book.id === id);
}

function printBookData(id: number): void {
    const book = getBook(id);
    if (book) {
        console.log(`Title: ${book.title}`);
        console.log(`Author: ${book.author}`);
        console.log(`Reading Status: ${book.readingStatus ? "Read" : "Unread"}`);
    } else {
        console.log(`Book with id ${id} not found.`);
    }
}

function printReadingStatus(author: string, title: string): void {
    const book = library.find(book => book.author === author && book.title === title);
    if (book) {
        console.log(`${book.title} by ${book.author} is ${book.readingStatus ? "read" : "unread"}`);
    } else {
        console.log(`Book "${title}" by ${author} not found.`);
    }
}

function addNewBook(author: string, title: string): void {
    const id = library.length + 1;
    library.push({
        author,
        title,
        readingStatus: false,
        id,
    });
    console.log(`New book "${title}" by ${author} added with id ${id}.`);
}

function readBook(id: number): void {
    const book = getBook(id);
    if (book) {
        book.readingStatus = true;
        console.log(`Marked "${book.title}" by ${book.author} as read.`);
    } else {
        console.log(`Book with id ${id} not found.`);
    }
}

function saveToJSON(library: Book[]): void {
    const data = JSON.stringify(library, null, 2);
    fs.writeFileSync("library.json", data);
    console.log("Library data saved to library.json.");
}

function loadFromJSON(library: Book[]): void {
    try {
        const data = fs.readFileSync("library.json", "utf-8");
        const parsedData = JSON.parse(data);
        if (Array.isArray(parsedData)) {
            library.length = 0;
            library.push(...parsedData);
            console.log("Library data loaded from library.json.");
        } else {
            console.log("Error: Invalid data format in library.json.");
        }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (error: any) {
        console.log("Error:", error.message);
    }
}

// Add a new book to the library
addNewBook("J.K. Rowling", "Harry Potter and the Philosopher's Stone");

// Print the data of a specific book
printBookData(4); // Assuming the newly added book got ID 4

// Mark a book as read
readBook(2); // Assuming we want to mark "Gödel, Escher, Bach" as read

// Print the reading status of a book
printReadingStatus("Harper Lee", "To Kill A Mockingbird");

// Save the library data to a JSON file
saveToJSON(library);

// Load the library data from the JSON file
loadFromJSON(library);
