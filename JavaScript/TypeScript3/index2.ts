// Exercise 7: type guards

function isNumberArray(arr: unknown[]): boolean {
    for (const elem of arr) {
        if (typeof elem !== "number") {
            return false;
        }
    }
    return true;
}

// Example 1: Array contains only numbers
const numbersArray1: unknown[] = [1, 2, 3, 4, 5];
console.log(isNumberArray(numbersArray1)); // Output: true

// Example 2: Array contains a mix of numbers and non-numbers
const numbersArray2: unknown[] = [1, "two", 3, "four", 5];
console.log(isNumberArray(numbersArray2)); // Output: false

// Example 3: Array is empty
const emptyArray: unknown[] = [];
console.log(isNumberArray(emptyArray)); // Output: true (since there are no non-number elements)
