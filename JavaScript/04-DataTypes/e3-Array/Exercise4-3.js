// Create an array of fruits and print the length of the array to the console.

const hedelmat = ["apple", "banana", "orange"];

console.log(hedelmat);
console.log(hedelmat.length);

// Try changing some of the values in the array and then printing the values to the console.

hedelmat[1] = "grape";
hedelmat[2] = "kiwi";
console.log(hedelmat);

// Add a value to the array using push -method

hedelmat.push("melon");
console.log(hedelmat);
console.log(hedelmat.length);

// The new value was added at the last position

// Remove a value from the array using pop -method
let removedElement = hedelmat.pop();
console.log(`${removedElement} was removed.`);
console.log(hedelmat.length);

// Try printing the whole array to the console.
console.log(hedelmat);

// Arrays are printed to the console showing their elements and their indices

// Check the type of your array using typeof and print the type to the console.
console.log(typeof hedelmat);
