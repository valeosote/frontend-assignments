// Exercise 1: Reading APIs

import axios, { AxiosResponse } from "axios";

export interface Post {
    userId: number;
    id: number;
    title: string;
    body: string;
  }

interface User {
    id: number;
    name: string;
}

async function getPostById(id: number): Promise<void> {
    try {
        // Fetch post by id
        const postResponse: AxiosResponse<Post> = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
        const postData: Post = postResponse.data;

        // Print post information
        console.log(`Post #${id}: ${postData.title}`);
        
        // Fetch user by userId
        const userId = postData.userId;
        const userResponse: AxiosResponse<User> = await axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`);
        const userData = userResponse.data;
        const listResponses: AxiosResponse<Array<Post>> = await axios.get(`https://jsonplaceholder.typicode.com/users/${userId}/posts`);
        //console.log(listResponses);
        //console.log(userData);
        const allPostsByUser: Array<Post> = listResponses.data;
        const ordinalOfPost = 1+allPostsByUser.map(p => p.id).findIndex(i => i===id);

        // Print post information again
        console.log(`Post #${ordinalOfPost} by ${userData.name}: ${postData.title}`);

    } catch (error) {
        console.error("Error:", error);
    }
}

getPostById(15);
