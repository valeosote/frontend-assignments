import axios, { AxiosResponse } from "axios";

interface Comment {
    postId: number
    id: number
    title: string
    name: string
    email: string
    body: string
}

async function getPostComments(id: number): Promise<void> {
    try {
        // Fetch comments for the given post id
        const response: AxiosResponse<Comment[]> = await axios.get(`https://jsonplaceholder.typicode.com/comments?postId=${id}`);
        const comments: Comment[] = response.data;

        // Print all comments
        comments.forEach((comment, index) => {
            console.log(`Comment ${index + 1}: ${comment.body}
                        `);
        });
    } catch (error) {
        console.error("Error:", error);
    }
}

getPostComments(15);
