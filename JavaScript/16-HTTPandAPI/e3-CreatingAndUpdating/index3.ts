import axios, { AxiosResponse } from "axios";
//import { Post } from "./index";

interface Post {
    userId: number;
    id: number;
    title: string;
    body: string;
}

async function createPost(): Promise<void> {
    try {
        const newPost: Post = {
            userId: 7,
            id: 1,
            title: "Hello There",
            body: "Your father was a great Jedi."
        };

        // let's send our new post item to the API
        const response: AxiosResponse<Post> = await axios.post<Post>("https://jsonplaceholder.typicode.com/posts", newPost);
        console.log("HTTP Status Code:", response.status);
    } catch (error) {
        console.error("Error:", error);
    }
}

async function updatePost(postId: number): Promise<void> {
    try {
        // first we fetch the existing post item
        const response: AxiosResponse<Post> = await axios.get<Post>(`https://jsonplaceholder.typicode.com/posts/${postId}`);
        const post: Post = response.data;

        // we modify the title of the existing post
        post.title = "Hello Again";

        // now we send the updated post item back to the API
        const updateResponse: AxiosResponse<Post> = await axios.put<Post>(`https://jsonplaceholder.typicode.com/posts/${postId}`, post);
        console.log("HTTP Status Code:", updateResponse.status);
    } catch (error) {
        console.error("Error:", error);
    }
}

createPost();
updatePost(1);
