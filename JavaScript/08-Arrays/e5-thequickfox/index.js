const listani = ["the", "quick", "brown", "fox"];

console.log(listani);

console.log("Second element:", listani[1]);
console.log("Third element:", listani[2]);

listani[2]="gray";

console.log("Third element is now:", listani[2]);

listani.push("over")
listani.push("lazy")
listani.push("dog")

console.log(listani);

listani.unshift("pangram:")

//console.log(listani);

listani.splice(5, 0, "jump");
listani.splice(7, 0, "the");

console.log(listani);

listani.shift();
listani.splice(-5);
listani.splice(2, 1); //removing "gray"

console.log(listani);