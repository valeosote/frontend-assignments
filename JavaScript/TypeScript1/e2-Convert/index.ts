// re-writing Exercise4.3 with a bit of TypeScript

const hedelmat: string[] = ["apple", "banana", "orange"];

console.log(hedelmat);
console.log(hedelmat.length);

hedelmat[1] = "grape";
hedelmat[2] = "kiwi";
console.log(hedelmat);

hedelmat.push("melon");
console.log(hedelmat);
console.log(hedelmat.length);

let removedElement: string | undefined = hedelmat.pop();
if (removedElement) {
    console.log(`${removedElement} was removed.`);
}
console.log(hedelmat.length);

console.log(hedelmat);

console.log(typeof hedelmat);
