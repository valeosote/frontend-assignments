const autot = [
    {
        rekisteri: "abc-123",
        merkki: "Toyota",
        vuosimalli: 1998,
    },
    {
        rekisteri: "xyz-999",
        merkki: "Mercedes Benz",
        vuosimalli: 2011,
    },
    {
        rekisteri: "les-0",
        merkki: "Tesla",
        vuosimalli: 2022,
    },
];

console.log(autot);

import { readFile } from "fs/promises";

async function readJSONFile(filePath) {
    try {
        const jsonData = await readFile(filePath, "utf8");
    
        const parsedData = JSON.parse(jsonData);
        
        console.log(parsedData);
    } catch (error) {
        console.error("Error reading file:", error);
    }
}

readJSONFile("autot.json");
