/* Fruit weight comparison
We have the following fruits:

a pear, weighting 178 grams
a lemon, weighting 120 grams
an apple, weighting 90 grams
a mango, weighting 150 grams

Create objects for each fruit, with the object containing the fruit's name and its weight.

a) Calculate the average weight of the fruits and print it.

b) Programmatically compare the weight of each fruit to the average weight of the fruits.

Print out the name of the fruit that has a weight closest to the average weight. */

const hedelmat = [
    { nimi: "pear", paino: 178 },
    { nimi: "lemon", paino: 120 },
    { nimi: "apple", paino: 90 },
    { nimi: "mango", paino: 150 }
];

// const yhteisPaino = hedelmat.reduce((acc, hedelma) => acc + hedelma.paino, 0);

const yhteisPaino = hedelmat[0].paino + hedelmat[1].paino + hedelmat[2].paino + hedelmat[3].paino;

const keskiPaino = yhteisPaino / hedelmat.length;

console.log(`Hedelmiä on ${hedelmat.length} kappaletta, ja niiden painojen keskiarvo on ${keskiPaino}.`);

let keskivertoHedelma = hedelmat[0];

if (hedelmat[1].paino <= keskiPaino){
    if (keskivertoHedelma.paino < hedelmat[1].paino ||
        keskivertoHedelma.paino + hedelmat[1].paino > 2*keskiPaino){
        keskivertoHedelma = hedelmat[1];
    }
} else if (keskivertoHedelma.paino > hedelmat[1].paino ||
           keskivertoHedelma.paino + hedelmat[1].paino < 2*keskiPaino){
        keskivertoHedelma = hedelmat[1];
}
if (hedelmat[2].paino <= keskiPaino){
    if (keskivertoHedelma.paino < hedelmat[2].paino ||
        keskivertoHedelma.paino + hedelmat[2].paino > 2*keskiPaino){
        keskivertoHedelma = hedelmat[2];
    }
} else if (keskivertoHedelma.paino > hedelmat[2].paino ||
           keskivertoHedelma.paino + hedelmat[2].paino < 2*keskiPaino){
        keskivertoHedelma = hedelmat[2];
}
if (hedelmat[3].paino <= keskiPaino){
    if (keskivertoHedelma.paino < hedelmat[3].paino ||
        keskivertoHedelma.paino + hedelmat[3].paino > 2*keskiPaino){
        keskivertoHedelma = hedelmat[3];
    }
} else if (keskivertoHedelma.paino > hedelmat[3].paino ||
           keskivertoHedelma.paino + hedelmat[3].paino < 2*keskiPaino){
        keskivertoHedelma = hedelmat[3];
}

console.log(`Hedelmä jonka paino on lähimpänä keskiarvoa on ${keskivertoHedelma.nimi}.`);