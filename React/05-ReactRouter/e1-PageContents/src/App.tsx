import { useState } from "react";
import About from "./About";
import Home from "./Home";
import Links from "./Links";
import "./App.css";

function App() {
  const [page, setPage] = useState("home");
  const dis = "Rölli";

  const toPage = (page: string) => (event: { preventDefault: () => void }) => {
    event.preventDefault();
    setPage(page);
  };

  const content = () => {
    if (page === "home") return <Home name={dis} />;
    else if (page === "about") return <About name={dis} />;
    else if (page === "links") return <Links name={dis} />;
  };

  return (
    <div className="container">
      <div className="linkbar">
        <a href="" onClick={toPage("home")} className="padding">
          {" "}
          home{" "}
        </a>
        <a href="" onClick={toPage("about")} className="padding">
          {" "}
          about{" "}
        </a>
        <a href="" onClick={toPage("links")} className="padding">
          {" "}
          links{" "}
        </a>
      </div>
      {content()}
    </div>
  );
}

export default App;
