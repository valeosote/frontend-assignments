//import { useState } from "react";

interface AboutProps {
  name: string;
}

export default function InputForm({name}: AboutProps) {

  return (
    <div>This is app was made for a React exercise. It shows the need for React Router. What do you think, {name}?</div>
  );
}
