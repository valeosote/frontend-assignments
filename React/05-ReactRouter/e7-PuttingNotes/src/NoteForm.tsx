import { useState } from "react";
import { NoteType } from "./Notes";
import axios from "axios";

interface NoteFormProps {
  setNotes: (notes: Array<NoteType>) => void;
  notes: Array<NoteType>;
}

function NoteForm({ setNotes, notes }: NoteFormProps) {
  const [noteText, setNoteText] = useState("");
  const [important, setImportant] = useState(false);

  const handleSubmit = (event: { preventDefault: () => void }) => {
    event.preventDefault();
    let newId = "1";
    while (notes.filter((note) => note.id === newId).length > 0) {
      newId = `${parseInt(newId) + 1}`;
    }
    const thedate = new Date().toISOString();
    const newNote: NoteType = {
      id: newId,
      content: noteText,
      date: thedate,
      important: important,
    };
    postToServer(newNote);
  };

  const postToServer = (note: NoteType) => {
    axios.post("http://localhost:3001/notes", note).then((response) => {
      setNotes([...notes, response.data]);
    });
  };

  return (
    <form className="ourForm" onSubmit={handleSubmit}>
      <textarea
        value={noteText}
        onChange={(event) => setNoteText(event.target.value)}
        placeholder="Enter a new note..."
        rows={2}
        cols={25}
      />
      <br />
      <label>
        Important:
        <input
          type="checkbox"
          checked={important}
          onChange={(event) => setImportant(event.target.checked)}
        />
      </label>
      <br />
      <input type="submit" value="Submit" />
    </form>
  );
}

export default NoteForm;
