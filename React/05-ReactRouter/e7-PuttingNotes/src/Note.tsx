import { NoteType } from "./Notes";

interface NoteProps {
  note: NoteType;
  removeNote: (id: string) => void;
  toggle: (id: string) => void;
}

const Note = ({
  note: { id, content, important },
  removeNote,
  toggle,
}: NoteProps) => {
  return (
    <div className={`notebox ${important ? "important" : ""}`}>
      <p>{content}</p>
      <div className="notebuttons">
        <button className="xbox" onClick={() => removeNote(id)}>
          X
        </button>
        <button className="togglebox" onClick={() => toggle(id)}>
          Toggle
        </button>
      </div>
    </div>
  );
};

export default Note;
