import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import Note from "./Note";
import NoteForm from "./NoteForm";

export interface NoteType {
  id: string;
  content: string;
  date: string;
  important: boolean;
}

function Notes() {
  const [notes, setNotes] = useState<Array<NoteType>>([]);

  useEffect(() => {
    axios
      .get<void, AxiosResponse>("http://localhost:3001/notes")
      .then((response) => {
        console.log(response.data);
        const incoming: Array<NoteType> = response.data;
        setNotes(incoming);
      })
      .catch((error) => {
        console.error("Error fetching notes:", error);
      });
  }, []);

  const removeNote = (id: string) => {
    const renotes = notes.filter((note) => note.id !== id);
    setNotes(renotes);
  };

  const toggleImportance = (id: string) => {
    //console.log('importance of ' + id + ' needs to be toggled')
    const foundNote = notes.find((n) => n.id === id);
    if (foundNote) {
      const changedNote = { ...foundNote, important: !foundNote.important };
      const url = `http://localhost:3001/notes/${id}`;
      axios.put(url, changedNote).then((response) => {
        setNotes(notes.map((note) => (note.id === id ? response.data : note)));
      });
    }
  };

  return (
    <div>
      <div className="note-container">
        {notes.map((note) => (
          <Note
            key={note.id}
            note={note}
            removeNote={removeNote}
            toggle={toggleImportance}
          />
        ))}
      </div>
      <NoteForm setNotes={setNotes} notes={notes} />
    </div>
  );
}

export default Notes;
