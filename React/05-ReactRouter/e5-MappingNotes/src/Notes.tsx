import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import Note from "./Note";

export interface NoteType {
  id: number;
  content: string;
  date: string;
  important: boolean;
}

function Notes() {
  const [notes, setNotes] = useState<Array<NoteType>>([]);

  useEffect(() => {
    axios
      .get<void, AxiosResponse>("http://localhost:3001/notes")
      .then((response) => {
        console.log(response.data);
        const incoming: Array<NoteType> = response.data;
        setNotes(incoming);
      })
      .catch((error) => {
        console.error("Error fetching notes:", error);
      });
  }, []);

  const removeNote = (id: number) => {
    const renotes = notes.filter((note) => note.id !== id);
    setNotes(renotes);
  };

  return (
    <div>
      {notes.map((note) => (
        <Note key={note.id} note={note} removeNote={removeNote} />
      ))}
    </div>
  );
}

export default Notes;
