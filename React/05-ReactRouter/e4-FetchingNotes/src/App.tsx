//import { useState } from "react";
import "./App.css";
import Notes from "./Notes";

function App() {

  return (
    <div className="container">
      <h2>Notes</h2>
        <Notes />
    </div>
  );
}

export default App;
