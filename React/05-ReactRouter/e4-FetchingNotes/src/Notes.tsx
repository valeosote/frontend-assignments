import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";

interface Note {
  id: number;
  content: string;
  date: string;
  important: boolean;
}

function Notes() {
  const [notes, setNotes] = useState<Array<Note>>([]);

  useEffect(() => {
    axios
      .get<void, AxiosResponse>("http://localhost:3001/notes")
      .then((response) => {
        console.log(response.data);
        const incoming: Array<Note> = response.data;
        setNotes(incoming);
      })
      .catch((error) => {
        console.error("Error fetching notes:", error);
      });
  }, []);

  return (
    <div>
      {notes.map((note) => (
        <div key={note.id} className="notebox">{note.content}</div>
      ))}
    </div>
  );
}

export default Notes;
