//import { useState } from "react";

interface HomeProps {
  name: string;
}

export default function InputForm({name}: HomeProps) {

  return (
    <div><h2>Hello there, {name}!</h2></div>
  );
}