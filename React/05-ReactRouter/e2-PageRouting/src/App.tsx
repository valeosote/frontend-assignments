import {  BrowserRouter as Router,  Routes, Route, Link} from "react-router-dom";
import About from "./About";
import Home from "./Home";
import Links from "./Links";
import "./App.css";


function App() {

  const dis = "Rölli";

  return (
    <div className="container">
      <Router>
      <div className="linkbar">
        <Link className="padding" to="/">home</Link>
        <Link className="padding" to="/links">links</Link>
        <Link className="padding" to="/about">about</Link>
      </div>

      <Routes>
        <Route path="/about" element={<About name={dis}/>} />
        <Route path="/links" element={<Links name={dis}/>} />
        <Route path="/" element={<Home name={dis}/>} />
      </Routes>
    </Router>
    </div>
  );
}

export default App;

