import { NoteType } from "./Notes";

interface NoteProps {
  note: NoteType;
  removeNote: (id: number) => void;
}

const Note = ({
  note: {id, content, important},
  removeNote
}: NoteProps) => {
  return (
    <div className={`notebox ${important ? "important" : ""}`}>
      <p>{content}</p>
      <button className="xbox" onClick={() => removeNote(id)}>
        X
      </button>
    </div>
  );
};

export default Note;
