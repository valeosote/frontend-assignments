import CounterButton from "./CounterButton";
import "./App.css";

function App() {
  return (
    <div className="ButtonContainer">
      <CounterButton />
      <CounterButton />
      <CounterButton />
    </div>
  );
}

export default App;
