import "bootstrap/dist/css/bootstrap.min.css"; //hmm, tämä ei ollutkaan react-bootstrap
import { useState } from "react";

const CounterButton = () => {
  const [buttonValue, setButtonValue] = useState(0);

  return (
    <button
      onClick={() => setButtonValue(buttonValue + 1)}
      type="button"
      className="btn btn-primary fs-4 px-4 py-2"
    >
      {buttonValue}
    </button>
  );
};

export default CounterButton;
