import { useState } from "react";

interface BingoTileProps {
  name: string;
  thisItemIsInRow: number;
  thisItemIsInCol: number;
  selectThis: (x: number, y: number) => void;
  revealBingo: boolean;
  partOfBingo: boolean;
}

function BingoTile({
  name,
  thisItemIsInRow,
  thisItemIsInCol,
  selectThis,
  revealBingo,
  partOfBingo,
}: BingoTileProps) {
  const [original, setOriginal] = useState(true);

  //console.log(`Hello from ${thisItemIsInRow},${thisItemIsInCol}.`);

  const HandleClick = () => {
    if (original) {
      selectThis(thisItemIsInCol, thisItemIsInRow);
    }
    setOriginal(!original);
  };

  return (
    <div
      className={`bingo-tile ${original ? "green-tile" : "red-tile"} ${
        partOfBingo && revealBingo ? "black-tile" : ""
      }`}
      onClick={() => HandleClick()}
    >
      {name}
    </div>
  );
}

export default BingoTile;
