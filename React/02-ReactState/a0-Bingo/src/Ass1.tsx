import { useState } from "react";

function Ass1() {
  const [isVisible, setIsVisible] = useState(false);

  return (
    <div>
      <h1>Assignment 1 - Toggle Render</h1>
      <div className="container">
        <button className="button" onClick={() => setIsVisible(!isVisible)}>
          Toggle Text
        </button>
        {isVisible && <p>Fear is the path to the Dark Side.</p>}
      </div>
    </div>
  );
}

export default Ass1;
