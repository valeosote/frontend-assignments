import { useState } from "react";

interface ToggleButtonProps {
  id: number;
}

function ToggleButton({ id }: ToggleButtonProps) {
  const [isVisible, setIsVisible] = useState(true);

  return (
    <div>
      {isVisible && (
        <button className="button" onClick={() => setIsVisible(!isVisible)}>
          Button {id}
        </button>
      )}
    </div>
  );
}

export default ToggleButton;
