import ToggleButton from "./ToggleButton";

function Ass2() {
  return (
    <div>
      <h1>Assignment 2 - More Toggle Render</h1>
      {[1, 2, 3, 4, 5].map((id) => (
        <ToggleButton id={id} key={id} />
      ))}
    </div>
  );
}

export default Ass2;
