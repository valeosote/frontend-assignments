import Ass1 from "./Ass1";
import Ass2 from "./Ass2";
import Bingo from "./Bingo";
import "./App.css";

function App() {
  const showAllThreeAssignments = true;

  if (showAllThreeAssignments) {
    return (
      <div>
        <Ass1 />
        <Ass2 />
        <Bingo />
      </div>
    );
  } else {
    return (
      <div>
        <Bingo />
      </div>
    );
  }
}

export default App;
