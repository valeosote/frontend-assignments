import { useState } from "react";

interface BingoSizeButtonProps {
  gridSize: number;
  setGridSize: () => void;
}

function BingoSizeButton({ gridSize, setGridSize }: BingoSizeButtonProps) {
  return (
    <div>
      <button type="button" className="button" onClick={() => setGridSize()}>
        {gridSize}x{gridSize}
      </button>
    </div>
  );
}

interface BingoShowButtonProps {
  maxInALine: number;
  toggleBingos: (show: boolean) => void;
  revealBingo: boolean;
}

function BingoShowButton({ maxInALine, toggleBingos, revealBingo }: BingoShowButtonProps) {
  const [isPushed, setIsPushed] = useState(!revealBingo);

  const handleBingoShow = () => {
    //console.log("click");
    setIsPushed(!isPushed);
    toggleBingos(isPushed);
  };

  return (
    <div>
      <button
        type="button"
        className={`button ${isPushed ? "pressedButton" : ""}`}
        onClick={() => handleBingoShow()}
      >
        {maxInALine}
      </button>
    </div>
  );
}

export { BingoSizeButton, BingoShowButton };
