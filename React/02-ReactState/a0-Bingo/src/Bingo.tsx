import { useState, useEffect, useCallback } from "react";
import { BingoSizeButton, BingoShowButton } from "./BingoButtons";
import BingoTile from "./BingoTile";
import { names } from "./Names";

//todo: unselecting tiles only changes the tile color, but internally it is still stored as a selected tile

/* const names = [
  "Anakin Skywalker",
  "Leia Organa",
  "Han Solo",
  "C-3PO",
  "R2-D2",
  "Darth Vader",
  "Obi-Wan Kenobi",
  "Yoda",
  "Palpatine",
  "Boba Fett",
  "Lando Calrissian",
  "Jabba the Hutt",
  "Mace Windu",
  "Padmé Amidala",
  "Count Dooku",
  "Qui-Gon Jinn",
  "Aayla Secura",
  "Ahsoka Tano",
  "Ki-Adi-Mundi",
  "Luminara Unduli",
  "Plo Koon",
  "Kit Fisto",
  "Shmi Skywalker",
  "Beru Whitesun",
  "Owen Lars",
]; */

class Tile {
  x: number;
  y: number;
  N: number;
  W: number;
  NW: number;
  NE: number;

  constructor(
    x: number,
    y: number,
    N: number,
    W: number,
    NW: number,
    NE: number
  ) {
    this.x = x;
    this.y = y;
    this.N = N;
    this.W = W;
    this.NW = NW;
    this.NE = NE;
  }

  setN(north: number) {
    this.N = north;
  }
  setW(west: number) {
    this.W = west;
  }
  setNW(NW: number) {
    this.NW = NW;
  }
  setNE(NE: number) {
    this.NE = NE;
  }
  set(value: number, directionX: number, directionY: number) {
    if (directionX === 0 && directionY === 1) {
      this.N = value;
    } else if (directionX === 1 && directionY === 0) {
      this.W = value;
    } else if (directionX === 1 && directionY === 1) {
      this.NW = value;
    } else if (directionX === -1 && directionY === 1) {
      this.NE = value;
    }
  }
  maks() {
    let m = this.N;
    if (m < this.W) {
      m = this.W;
    }
    if (m < this.NW) {
      m = this.NW;
    }
    if (m < this.NE) {
      m = this.NE;
    }
    return m;
  }
}

const shuffleArray = (array: Array<string>) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};

const bingoSize = 5;
const flatArray: string[] = shuffleArray(names.slice(0, bingoSize * bingoSize));
const itemGrid: string[][] = [];

while (flatArray.length) {
  itemGrid.push(flatArray.splice(0, bingoSize));
}

function Bingo() {
  const [gridSize, setGridSize] = useState(bingoSize);
  const [maxInALine, setMaxInALine] = useState(0);
  const [revealBingo, setRevealBingo] = useState(true);
  const [bingoAnnounced, setBingoAnnounced] = useState(false);
  const [knownBingoTiles, setKnownBingoTiles] = useState<Array<number>>([]);
  const [showValues, setShowValues] = useState(false);
  const [selectedTiles, setSelectedTiles] = useState<Array<Tile>>([]);

  const handleAddingASelectedTile = (x: number, y: number) => {
    let [N, W, NW, NE] = [1, 1, 1, 1];
    let largestEncounteredValue = 0;

    const Ntile = selectedTiles.find(
      (tile) => tile.x === x && tile.y === y - 1
    );
    if (Ntile) {
      N = Ntile.N + 1;
      //console.log(`(${x},${y}) found N: (${x},${y - 1})`);
    }
    const Wtile = selectedTiles.find(
      (tile) => tile.x === x - 1 && tile.y === y
    );
    if (Wtile) {
      W = Wtile.W + 1;
      //console.log(`(${x},${y}) found W: (${x - 1},${y})`);
    }
    const NWtile = selectedTiles.find(
      (tile) => tile.x === x - 1 && tile.y === y - 1
    );
    if (NWtile) {
      NW = NWtile.NW + 1;
      //console.log(`(${x},${y}) found NW: (${x - 1},${y - 1})`);
    }
    const NEtile = selectedTiles.find(
      (tile) => tile.x === x + 1 && tile.y === y - 1
    );
    if (NEtile) {
      NE = NEtile.NE + 1;
      //console.log(`(${x},${y}) found NE: (${x + 1},${y - 1})`);
    }
    const newTile = new Tile(x, y, N, W, NW, NE);
    if (newTile.maks() > largestEncounteredValue) {
      largestEncounteredValue = newTile.maks();
    }

    const searchFollowers = (
      xx: number,
      yy: number,
      xd: number,
      yd: number,
      fValue: number
    ) => {
      let fTileInd = -1;
      do {
        fTileInd = selectedTiles.findIndex(
          (tile) => tile.x === xx + xd && tile.y === yy + yd
        );
        if (fTileInd > -1) {
          xx += xd;
          yy += yd;
          fValue++;
          const fTile = selectedTiles[fTileInd];
          fTile.set(fValue, xd, yd);
          if (fTile.maks() > largestEncounteredValue) {
            largestEncounteredValue = fTile.maks();
          }
          //console.log(`(${xx},${yy}) sighted from (${xx - xd},${yy - yd})`);
        }
      } while (fTileInd > -1);
      if (largestEncounteredValue > maxInALine) {
        setMaxInALine(largestEncounteredValue);
      }
      if (largestEncounteredValue >= bingoSize) {
        doWeShowAllBingos(revealBingo);
        if (!bingoAnnounced) {
          console.log("BINGO!");
          setBingoAnnounced(true);
        }
      }
    };

    searchFollowers(x, y, 0, 1, N);
    searchFollowers(x, y, 1, 0, W);
    searchFollowers(x, y, 1, 1, NW);
    searchFollowers(x, y, -1, 1, NE);

    setSelectedTiles([...selectedTiles, newTile]);
  };

  const doWeShowAllBingos = useCallback((show: boolean) => {
    //console.log("TOGGLE");
    setRevealBingo(show);
    const fives = selectedTiles
      .map((tile, index) => ({ maks: tile.maks(), index }))
      .filter(({ maks }) => maks >= bingoSize)
      .map(({ index }) => index);
    const partials: number[] = [];

    const markBingoTiles = (
      indexOfSelectedTilesToWorkFrom: number,
      xd: number,
      yd: number
    ) => {
      let fTileInd = -1;
      let xx = selectedTiles[indexOfSelectedTilesToWorkFrom].x;
      let yy = selectedTiles[indexOfSelectedTilesToWorkFrom].y;
      do {
        fTileInd = selectedTiles.findIndex(
          (tile) => tile.x === xx + xd && tile.y === yy + yd
        );
        if (fTileInd > -1) {
          xx += xd;
          yy += yd;
          partials.push(fTileInd);
        }
      } while (fTileInd > -1);
    };

    for (const n of fives) {
      if (selectedTiles[n].N >= bingoSize) {
        markBingoTiles(n, 0, -1);
      }
      if (selectedTiles[n].W >= bingoSize) {
        markBingoTiles(n, -1, 0);
      }
      if (selectedTiles[n].NW >= bingoSize) {
        markBingoTiles(n, -1, -1);
      }
      if (selectedTiles[n].NE >= bingoSize) {
        markBingoTiles(n, 1, -1);
      }
    }

    setKnownBingoTiles([...new Set([...fives, ...partials])]);
  }, [selectedTiles, setRevealBingo, setKnownBingoTiles]);

  useEffect(() => {
    doWeShowAllBingos(revealBingo);
  }, [doWeShowAllBingos, revealBingo, selectedTiles]);

  const handleGridSizeEnlargement = () => {
    const incomingItems = shuffleArray(
      names.slice(gridSize * gridSize, (gridSize + 1) * (gridSize + 1))
    );
    itemGrid.push(incomingItems.splice(0, gridSize));
    for (let i = 0; i < itemGrid.length; i++) {
      itemGrid[i].push(incomingItems[i]);
    }
    setGridSize(gridSize + 1);
  };

  const internalValues: number[][][] = itemGrid.map((row, rowIndex) =>
    row.map((_, columnIndex) => {
      const laugh = selectedTiles.find(
        (tile) => tile.x === columnIndex && tile.y === rowIndex
      );
      if (laugh) {
        return [laugh.N, laugh.W, laugh.NW, laugh.NE];
      } else {
        return [0];
      }
    })
  );

  return (
    <div>
      <div className="rowofthings">
        <h1>Assignment 3 - Bingo</h1>
        <div className="bingobuttons">
          <div>
            <button
              type="button"
              className={`button ${showValues ? "pressedButton" : ""}`}
              onClick={() => setShowValues(!showValues)}
            >
              #
            </button>
          </div>
          <BingoShowButton //pressing this button hides bingos, if there are any
            maxInALine={maxInALine}
            toggleBingos={doWeShowAllBingos}
            revealBingo={revealBingo}
          />
          <BingoSizeButton
            gridSize={gridSize}
            setGridSize={handleGridSizeEnlargement}
          />
        </div>
      </div>
      <div className="bingo-container">
        {itemGrid.map((row, rowIndex) => (
          <div key={rowIndex} className="bingo-row">
            {row.map((item, columnIndex) => (
              <BingoTile
                name={`${item}
                ${
                  showValues ? internalValues[rowIndex][columnIndex] : ""
                }`}
                thisItemIsInRow={rowIndex}
                thisItemIsInCol={columnIndex}
                selectThis={handleAddingASelectedTile}
                revealBingo={revealBingo}
                partOfBingo={selectedTiles.some(
                  (tile, index) =>
                    tile.x === columnIndex &&
                    tile.y === rowIndex &&
                    knownBingoTiles.includes(index)
                )}
                key={`${columnIndex}-${rowIndex}`}
              />
            ))}
          </div>
        ))}
      </div>
    </div>
  );
}

export default Bingo;
