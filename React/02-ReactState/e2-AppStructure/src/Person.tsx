import "./Person.css";

export interface PersonInterface {
    name: string;
    age: number;
}

interface PersonProps {
  chap: PersonInterface;
}

const Person = ({chap:{name, age}}: PersonProps) => {
  return (
    <div className="person"><p>{name} is {age} years old.</p></div>
  )
}

export default Person;