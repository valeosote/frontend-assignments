import Family from "./Family";
import "./App.css";

const people = [
  { name: 'Alice', age: 30 },
  { name: 'Bob', age: 35 },
  { name: 'Charlie', age: 40 },
  { name: 'Donna', age: 45 },
];

function App() {
  return (
    <div className="container">
      <Family chaps={people}/>
    </div>
  )
}

export default App;