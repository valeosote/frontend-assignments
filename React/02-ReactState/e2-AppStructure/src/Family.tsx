import Person, { PersonInterface as PersonType } from "./Person";
import "./Family.css";

interface FamilyProps {
  chaps: Array<PersonType>;
}

const Family = ({chaps}: FamilyProps) => {
  return (
    <div>
      <h3>Our Family</h3>
      {chaps.map((chap, index) =>
        <Person chap={chap} key={index}/>
      )}
    </div>
  )
}

export default Family;