import Jelly from "./Jelly";

const whatMinute = new Date().getMinutes();
const whatSecond = new Date().getSeconds();

const App = () => (
  <div>
    <Jelly firstNumber={whatMinute} secondNumber={whatSecond}/>
  </div>
);

export default App;