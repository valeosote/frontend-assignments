interface JellyProps {
  firstNumber: number;
  secondNumber: number;
}

const Jelly = ({firstNumber, secondNumber}: JellyProps) => {

    const sumBoth = firstNumber + secondNumber;
    const productSize = Math.floor(firstNumber * secondNumber / 100) * 100;
    const minOne = firstNumber < secondNumber ? firstNumber : secondNumber;
    const minAtMostSize = minOne > 49 ? 59 : Math.ceil((minOne)/10)*10;
    //const maxOne = firstNumber > secondNumber ? firstNumber : secondNumber;
    const maxOneDivisibleByThree = (sumBoth-minOne) % 3 === 0 ? "is" : "isn't";

    console.log(firstNumber, secondNumber);

    return (
      <div>
        <p>I'm thinking of two numbers, both between 0 and 59.</p>
        <p>The sum of the numbers is {sumBoth}.</p>
        <p>The product of the numbers is between {productSize} and {productSize+100}.</p>
        <p>The smaller number is between {minAtMostSize-10} and {minAtMostSize}.</p>
        <p>The larger number {maxOneDivisibleByThree} divisible by three.</p>
        <p>Can you guess the numbers?</p>
      </div>
    )
  }

export default Jelly;