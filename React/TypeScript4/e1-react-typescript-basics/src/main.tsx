//import React from 'react';
import ReactDOM from 'react-dom/client';

const App = () => (
  <div>
    <p>Hello world</p>
  </div>
);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(<App />);