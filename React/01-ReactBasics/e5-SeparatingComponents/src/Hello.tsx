import Year from "./Year";

interface HelloProps {
  itsTheYear: number;
}

const Hello = ({itsTheYear}: HelloProps) => {
    return (
      <div>
        <p>Hello world!</p>
        <Year giveMeAYear={itsTheYear}/>
      </div>
    )
  }

export default Hello;