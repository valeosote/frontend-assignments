import React from "react";

interface YearProps {
    giveMeAYear: number;
  }

const Year: React.FC<YearProps> = ({giveMeAYear}) => {
    return (
      <div>
        <p>It's the year {giveMeAYear}!</p>
      </div>
    )
  }

export default Year;