import Hello from "./Hello";

const itsYear = new Date().getFullYear();

const App = () => (
  <div>
    <Hello itsTheYear={itsYear}/>
  </div>
)
export default App