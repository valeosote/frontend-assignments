import NameList from "./NameList";

const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];

const App = () => (
  <div>
    <NameList nameList={namelist}/>
  </div>
)
export default App