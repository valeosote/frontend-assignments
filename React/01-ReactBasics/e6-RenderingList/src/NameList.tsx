import React from "react";

interface NameListProps {
  nameList: Array<string>;
}

const NameList: React.FC<NameListProps> = ({nameList}) => {
    return (
      <ul>
        {nameList.map((name, index) => (
        <li key={index} style={{ fontWeight: index % 2 === 0 ? 'bold' : 'normal',
                                  fontStyle: index % 2 === 1 ? 'italic' : 'normal' }}>
          {name}
        </li>
      ))}
      </ul>
    )
  }

export default NameList;
