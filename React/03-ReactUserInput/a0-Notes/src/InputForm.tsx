import { useState } from "react";

interface InputFormProps {
  addTodo: (text: string) => void;
}

export default function InputForm({ addTodo }: InputFormProps) {
  const [newTodo, setNewTodo] = useState("");

  const onSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    addTodo(newTodo);
    setNewTodo("");
  };

  return (
    <form className="ourForm" onSubmit={onSubmit}>
      <input
        type="text"
        value={newTodo}
        onChange={(e) => setNewTodo(e.target.value)}
      />
      <input type="submit" value="Submit" />
    </form>
  );
}
