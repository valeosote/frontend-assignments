import { useState } from "react";
import TodoNote from "./TodoNote";
import InputForm from "./InputForm";
import "./App.css";

const defaultTodos = [
  { id: 1, text: "Buy potatoes", complete: false },
  { id: 2, text: "Make food", complete: false },
  { id: 3, text: "Exercise", complete: false },
  { id: 4, text: "Do the dishes", complete: false },
  { id: 5, text: "Floss teeth", complete: false },
  { id: 6, text: "Play videogames", complete: true },
];

function App() {
  const [todos, setTodos] = useState(defaultTodos);

  const toggleComplete: (id: number) => void = (id) => {
    setTodos((todos) =>
      todos.map((todo) =>
        todo.id === id ? { ...todo, complete: !todo.complete } : todo
      )
    );
  };

  const removeTodo = (id: number) => {
    const retodos = todos.filter((todo) => todo.id !== id);
    setTodos(retodos);
  };

  const addTodo = (text: string) => {
    let id = 0;
    while (todos.some(note => note.id===id)) {
      id++;
    }
    const retodos = todos.concat({ id, text, complete: false });
    setTodos(retodos);
  };

  return (
    <div className="container">
      <h2>Notes</h2>
      <InputForm addTodo={addTodo}/>
      <div className="note-container">
        {todos.map((item) => (
          <TodoNote
            key={item.id}
            item={item}
            toggleComplete={toggleComplete}
            removeTodo={removeTodo}
          />
        ))}
      </div>
    </div>
  );
}

export default App;
