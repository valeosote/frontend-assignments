interface TodoNoteProps {
  item: { id: number; text: string; complete: boolean };
  toggleComplete: (id: number) => void;
  removeTodo: (id: number) => void;
}

const TodoNote = ({
  item: { id, text, complete },
  toggleComplete,
  removeTodo
}: TodoNoteProps) => {
  return (
    <div className={`notebox ${complete ? "complete" : ""}`}>
      <input
        type="checkbox"
        checked={complete}
        onChange={() => toggleComplete(id)}
      />
      <p>{text}</p>
      <button className="xbox" onClick={() => removeTodo(id)}>
        X
      </button>
    </div>
  );
};

export default TodoNote;
