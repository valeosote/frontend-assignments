import { useState } from "react";
import "./App.css";

function App() {
  const [inputText, setInputText] = useState<string>("");
  const [displayText, setDisplayText] = useState<string>("");

  const handleSubmit = () => {
    setDisplayText(inputText);
    setInputText("");
  };

  return (
    <div>
      <h2>Your text is: {displayText}</h2>
      <input
        type="text"
        value={inputText}
        onChange={(e) => setInputText(e.target.value)}
      />
      <button onClick={handleSubmit}>Submit</button>
    </div>
  );
}

export default App;
