function startCounter() {
    let seconds = 0;
    let minutes = 0;
    
    const timerElement = document.getElementById('timer');

    setInterval(() => {
        seconds++;
        if (seconds === 60) {
            seconds = 0;
            minutes++;
        }

        const timeString = `${minutes > 0 ? minutes + ' minute' + (minutes > 1 ? 's' : '') + ', ' : ''}${seconds} second${seconds !== 1 ? 's' : ''}`;

        timerElement.textContent = timeString;
    }, 1000);
}
