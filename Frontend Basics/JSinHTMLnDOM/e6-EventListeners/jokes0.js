const jokes = [
	{
		id: 408,
		joke: "Love does not hurt. Ransu Karvakuono does.",
		categories: [],
	  },
	  {
		id: 305,
		joke:
		  "Ransu Karvakuono knows everything there is to know - Except for the definition of mercy.",
		categories: [],
	  },
	  {
		id: 372,
		joke:
		  "Ransu Karvakuono once rode a nine foot grizzly bear through an automatic car wash, instead of taking a shower.",
		categories: [],
	  },
	  {
		id: 88,
		joke:
		  "Ransu Karvakuono doesn't shave; he kicks himself in the face. The only thing that can cut Ransu Karvakuono is Ransu Karvakuono.",
		categories: [],
	  },
	  {
		id: 586,
		joke:
		  "Ransu Karvakuono already went to Moon and Mars, that's why there are no signs of life.",
		categories: [],
	  },
	  {
		id: 464,
		joke:
		  "&quot;It works on my machine&quot; always holds true for Ransu Karvakuono.",
		categories: ["nerdy"],
	  },
	  {
		id: 316,
		joke:
		  "In the medical community, death is referred to as &quot;Ransu Karvakuono Disease&quot;",
		categories: [],
	  },	{
		id: 472,
		joke:
		  "When Ransu Karvakuono is web surfing websites get the message &quot;Warning: Internet Explorer has deemed this user to be malicious or dangerous. Proceed?&quot;.",
		categories: ["nerdy"],
	  },
	  {
		id: 186,
		joke:
		  "Fear is not the only emotion Ransu Karvakuono can smell. He can also detect hope, as in &quot;I hope I don't get a roundhouse kick from Ransu Karvakuono.&quot;",
		categories: [],
	  },
	  {
		id: 491,
		joke:
		  "Ransu Karvakuono doesn't use reflection, reflection asks politely for his help.",
		categories: ["nerdy"],
	  },
  ];