function load() {
    console.log("Page loaded!");
    //console.log(jokes[0]);
  }
  
window.onload = load;

const displayedJokes = [];
let untouchedJokes = [...jokes];

function handleAddingRandomJoke(listToPickFrom) {
  const randomJoke = listToPickFrom[Math.floor(Math.random() * listToPickFrom.length)];
  displayedJokes.push(randomJoke);
  addJokeToDocument(randomJoke);
  untouchedJokes = untouchedJokes.filter(joke => joke !== randomJoke);
  attachBoxEventListeners();
}

document.getElementById("btn1-joke").addEventListener("click", function() {
  handleAddingRandomJoke(untouchedJokes);
  console.log("A random joke was added.");
});

document.getElementById("btn2-nerdy").addEventListener("click", function() {
  const nerdyJokes = untouchedJokes.filter(joke => joke.categories.includes("nerdy"));
  handleAddingRandomJoke(nerdyJokes);
  console.log("A nerdy joke was added.");
});

document.getElementById("btn3-all").addEventListener("click", function() {
  while (untouchedJokes.length > 0) {
    handleAddingRandomJoke(untouchedJokes);
  }
  console.log("Added the rest of the jokes.");
});

document.getElementById("btn4-delete").addEventListener("click", function() {
  const selectedJoke = document.querySelector("#jokecontainer .selected");
  const firstJoke = document.querySelector("#jokecontainer .box");
  const deleteThisJoke = selectedJoke ? selectedJoke : firstJoke;
  if (deleteThisJoke) {
    //console.log(firstJoke);
    const convertedJoke = extractJokeFromDiv(deleteThisJoke);
    untouchedJokes.push(convertedJoke);
    deleteThisJoke.remove();
    console.log("A joke was deleted.");
  } else {
    console.log("No joke found to delete.");
  }
});

const jokeContainer = document.getElementById("jokecontainer");

function addJokeToDocument(joke) {
    const div = document.createElement("div");
    div.classList.add("box");
    for (let i = 0; i < joke.categories.length; i++) {
      div.classList.add(joke.categories[i]);
    }
    const jokeText = joke.joke.replace(/&quot;/g, '"');
    div.innerHTML = jokeText;
    jokeContainer.appendChild(div);
}

function extractJokeFromDiv(div) {
  const joke = {
      id: Math.floor(Math.random() * 1000+1000), //vähän laiskasti tämä kun ei ole oleellinen osa
      joke: div.innerHTML,
      categories: [...div.classList].filter(className => className !== "box"),
  };
  joke.joke = joke.joke.replace(/"/g, "&quot;");

  return joke;
}

function handleBoxSelection(box) {
  const isSelected = box.classList.contains("selected");

  document.querySelectorAll(".box").forEach(box => {
      box.classList.remove("selected");
  });
  box.classList.add("selected");
}

function attachBoxEventListeners() {
  document.querySelectorAll(".box").forEach(box => {
      box.addEventListener("click", () => {
          handleBoxSelection(box);
      });
  });
}

attachBoxEventListeners();