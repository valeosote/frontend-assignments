function load() {
    console.log("page loaded!");
  }
  
window.onload = load;

const colorchangers = document.querySelectorAll(".colorchanger");

colorchangers.forEach(function(element) {
  element.addEventListener("mouseenter", function() {
    this.classList.add("yellow");
  });

  element.addEventListener("mouseleave", function() {
    this.classList.remove("yellow")
  });
});
