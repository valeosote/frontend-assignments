function load() {
    console.log("page loaded!");
  }
  
window.onload = load;

const loginForm = document.querySelector("#namer");

loginForm.addEventListener("submit", (e) => {
  e.preventDefault();

  const username = document.querySelector("#username").value;

  console.log("username: ", username);

  const greetingElement = document.getElementById("greeting");
  greetingElement.textContent = `Howdy, ${username}!`;

  const questionElement = document.getElementById("question");
  questionElement.textContent = `Dare to change your name?`;
});
