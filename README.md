# Frontend - assignments

## Description
Here is an assortment of assignments and exercises I worked through in the spring of 2024.

## Project status
I plan to soon add some highlight or showcase exercises so it will be easier to find the more interesting examples.
